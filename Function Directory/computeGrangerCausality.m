function [Devtotal GCJL  GCJH  wftotal  sig2ftotal  sig2rtotal  robstotal  rhatftotal  rhatrtotal  Bftotal  Brtotal ...
    cellids  gammaOpttotal Whc Whcv Whcs gammaOpt Jcost] = computeGrangerCausality(DFF, Ncells, cellids, gammacv, ...
    alpha, Md, WH, Mhc, Mhcs, LL, LR,LLcv, LRcv, Mlow)

Devtotal = zeros(Ncells,Ncells,1);
DevtotalC = cell(Ncells,1);
DLLtotal = Devtotal;
wftotal = cell(Ncells,1);
robstotal = cell(Ncells,1);
sig2ftotal = zeros(Ncells,1);
sig2rtotal = zeros(Ncells,Ncells,1);
sig2rtotalC = cell(Ncells,1);
sig2rtotalC = cell(Ncells,1);
Bftotal = zeros(Ncells,1);
Brtotal = zeros(Ncells,Ncells,1);
rhatftotal = cell(Ncells,1);
rhatrtotal = cell(Ncells,Ncells,1);
rhatrtotalC = cell(Ncells,1);
gammaOpttotal = zeros(Ncells,1);

%Selected cells
DFF = single(DFF(:,:,cellids));
[Nframes,Ntrials,Ncells] = size(DFF);

% Cross-Hist Kernel: make sure that the length latency of interaction + latency of Ca maging
Whc = [1 WH*ones(1,Mhc-1)];
Lhc = sum(Whc);
Whcv = {Whc};

% Self-Hist Kernel
Whcs = [1 WH*ones(1,Mhcs-1)];
Whscv = {Whcs};
Lhcs = sum(Whcs);
Mf = (Ncells-1)*Mhc+Mhcs;
Mr = (Ncells-2)*Mhc+Mhcs;

% Cross-Validation
[gammaOpt,Jcost] = CrossValidModTrial2(DFF,gammacv,{Whc},{Whcs},LLcv,LRcv);
gammaOpttotal(:,1) = gammaOpt;

% Form Cross-History Covariate Matrices
Lm = max([Lhc,Lhcs]);
Np = Nframes - Lm;
Xcz = single(zeros([Np,Mhc,Ncells,Ntrials],'single'));
for r = 1:Ntrials
    %size [Nframes x Ncells]
    Xcz(:,:,:,r) = FormHistMatrixv2(squeeze(DFF(:,r,:)),Whc,Lm);
end

% Form Observation arrays for all cells
Robs = DFF(Lm+1:end,:,:);

% Zero-mean the observation vectors. This will remove the need for intercept (mu) estimation
for cc = 1:Ncells
    Robs(:,:,cc) = Robs(:,:,cc) - ones(Np,1)*mean(Robs(:,:,cc));
end
for ct = 1:Ncells
  
  fprintf(['Estimating G-Causality to cell ', num2str(ct), ' ...\n'])
  cellC = 1:Ncells; cellC(ct) = [];
  gamma = gammaOpt(ct);
  
  % Form Self-History Covriate matrix
  Xcsz = zeros([Np,Mhc,Ntrials],'single');
  for r = 1:Ntrials
    % size [Nframes x Ncells]
    Xcsz(:,:,r) = FormHistMatrixv2(squeeze(DFF(:,r,ct)),Whcs,Lm);
  end
  
  % Form Effective response vector
  robsz = Robs(:,:,ct);
  reff = robsz(:); Neff = numel(reff);
  
  % Full Model
  % Form standardized Full Model Covariate Matrix
  [Xneff,Dnf] = FormFullDesignv3(Xcz,Xcsz,ct);
  
  % Filtering/Estimation Setting
  [wnf,sig2f,Devf,Bf] = StaticEstim(Xneff,reff,gamma,LL,LR);
  
  % Save Tuning AR Coeffs full model for each cell
  wftotal{ct,1} = wnf;
  sig2ftotal(ct,1) = sig2f;
  robstotal{ct,1} = robsz;
  
  % Reconstructed observation vector full model
  rhatf = zeros(Np,Ntrials);
  for r = 1:Ntrials
    Xnf = Xneff((r-1)*Np+1:r*Np,:);
    rhatf(:,r) = Xnf*wnf;
  end
  rhatftotal{ct} = rhatf;
  Bftotal(ct) = Bf;
  
  [Devd,rhatr_tmp, sig2rtotal_tmp,Br] = LF_estimGC(cellC,Mhc,Mhcs,gamma,LL,LR,Xneff,reff,Np,Ntrials,Devf);
  
  sig2rtotalC{ct} = sig2rtotal_tmp;
  BrtotalC{ct} = Br;
  DevtotalC{ct} = Devd;
  rhatrtotalC{ct} = rhatr_tmp;
end

%Properly insert results into output variables using cellC
for ct = 1:Ncells
  cellC = 1:Ncells; cellC(ct) = [];
  sig2rtotal(ct,cellC) = sig2rtotalC{ct};
  Brtotal(ct,cellC) = BrtotalC{ct};
  Devtotal(ct,cellC) = DevtotalC{ct};
  for iC=1:length(cellC)
    rhatrtotal{ct,cellC(iC)} = rhatrtotalC{ct};
  end
end

% Statistical Test: J-statistics based on FDR
DkSmth = Devtotal-Md;
[GCJL,GCJH] = FDRcontrolBHv5(Devtotal,DkSmth,alpha,Md,wftotal,Whc,Mlow);

function [Devd,rhatr_tmp,sig2rtotal_tmp,Br] = LF_estimGC(cellC,Mhc,Mhcs,gamma,LL,LR,Xneff,reff,Np,Ntrials,Devf)

Br = zeros(size(cellC));
for iC = 1:length(cellC)
  cf = cellC(iC);
  
  % Reduced Model
  Xnefr = Xneff;
  cc = find(cellC == cf);
  Xnefr(:,(cc-1)*Mhc+Mhcs+1:cc*Mhc+Mhcs) = [];
  
  % Estimation Setting
  [wnr,sig2r,Devr,Br(iC)] = StaticEstim(Xnefr,reff,gamma,LL,LR);
  
  % Save estimated params/arrays
  sig2rtotal_tmp(iC) = sig2r;
  
  % Reconstructed observation vector reduced model
  rhatr = zeros(Np,Ntrials);
  for r = 1:Ntrials
    Xnr = Xnefr((r-1)*Np+1:r*Np,:);
    rhatr(:,r) = Xnr*wnr;
  end
  rhatr_tmp{iC} = rhatr;
  
  % Granger Causality metric: Compute Difference of Deviance Statistic
  Devd(iC) = Devf - Devr;
  %DB = Bf - Br;
  %DLL = Devd - DB;
  %DLLtotal(ct,cf,1) = DLL;
  
end

