function [wn,sig2] = StaticEstimCV(Xn,robs,gamma,LL,LR)

%%% Static Estimation of Sparse Tuning Coefficients (including Cross-History)
%%% Feb 20th 2017

% schid: Scheme ID

[N,M] = size(Xn);
%muhat = zeros(LR,1);

%%% Estimate the variance using ML estimate
XXn = Xn'*Xn;

Xdn = Xn'*robs;
wML = XXn\Xdn;
sig2ML = (norm(robs - Xn*wML)^2)/N;
% Set Initials based on ML estimate
% muML = mean(robs);
% zx = sum(Xn)';
% wML = XXn\(Xdn-muML*zx);
% sig2ML = (norm(robs -muML - Xn*wML)^2)/N;

% Set Initials based on ML estimate
sig2l = sig2ML;
wl = wML;

sig2l_old = inf;
for ll = 1:LR
    XXl = XXn/sqrt(sig2l*N*log(M));
    Xdl = Xdn/sqrt(sig2l*N*log(M));     
    % Step-size Selection
    rhom = max(abs(eig(XXl)));
    %al = 0.9/rhom;
    al = 0.9/rhom; alg = al*gamma;
    wl_old = inf;
    for l = 1:LL
      %     gl = (Xdl - XXl*wl);
      %     x = wl + al*gl;
      %     wl = SoftThreshold(wl + al*gl ,gamma*al);
      x = wl + al*(Xdl - XXl*wl);
      wl = sign(x).*max(abs(x)-alg,0);
      
      if norm(wl - wl_old)<0.0001; break; end
      wl_old = wl;
    end
    % Compute Variance
    sig2l = (norm(robs - Xn*wl)^2)/N;
    if norm(sig2l-sig2l_old) < 0.0001; break; end
    sig2l_old = sig2l;
end

wn = wl; sig2 = sig2l;
        
end









