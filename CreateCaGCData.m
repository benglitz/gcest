function [State,CM] = CreateCaGCData(varargin)

P = parsePairs(varargin);
checkField(P,'Duration',1000);
checkField(P,'NTrials',2);
checkField(P,'SR',2);
checkField(P,'Tau',3);
checkField(P,'NCells',10);
checkField(P,'NoiseSNR',0.2)
checkField(P,'Sparse',0);
checkField(P);

% Create Random Connectivity Matrix
CM = randn(P.NCells);
if P.Sparse CM = (abs(CM)>0.5).*sign(CM); end
CM = 0.5/P.NCells*CM;
for i=1:P.NCells CM(i,i) = exp(-1/P.SR / P.Tau); end

NSteps = round(P.Duration*P.SR);
State = zeros(NSteps,P.NCells,P.NTrials);
for iTT = 1:P.NTrials
  State(1,:,iTT) = rand(P.NCells,1);
  for iT = 1:NSteps-1
    if mod(iT,100)==1 State(iT,:,iTT) = State(iT,:,iTT) + round(1*rand(1,P.NCells)); end
    State(iT+1,:,iTT) = State(iT,:,iTT)*CM;
  end
end
SignalStd = std(State(:));
State = State + SignalStd*P.NoiseSNR*randn(size(State));
State = permute(State,[1,3,2]);